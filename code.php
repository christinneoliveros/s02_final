<!-- 
	Array Manipulation

	Activity Instruction:

	1. Create a function called 'createProduct' that accepts a name of the product and price as its parameter and add it to an array called 'products'

	2. Create a function called 'printProducts' that prints or displays the lists of elements of products array on an unordered list in the index.php

	3. Create a function called 'countProducts' that displays the count of the elements on a products array in the index.php

	4. Create a function called 'deleteProduct' that deletes an element to a products array
-->
<?php

$products = [];

function createProduct($name, $price){
		
		global $products; // we are telling the createProduct() that we are going to use a global variable called $products and modify on its back
		array_push($products, ['name' => $name, 'price' => $price]);
	}
	

function printProducts(){
	global $products;

	foreach ($products as $product) {
		echo "<li>";
		foreach ($product as $label => $value) {
			echo "$label : $value";
		}
		echo "</li>";
	}
}

function countProducts(){
	global $products;
	echo count($products);
}

function deleteProduct(){
	global $products;
	array_pop($products);

}

?>